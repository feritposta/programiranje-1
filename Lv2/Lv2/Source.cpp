#include <stdio.h>

//1. zadatak
//BTW program ispisuje sve neparne brojeve do unesenog broja
//int main(void)
//{
//	int w, c;
//	
//	do { scanf("%d", &w); } while (w <= 0); //mora biti tocka zarez nakon while i nedostaje adresni operator u scanf
//	for (c = 1; c <= w; c++) //u for ide tocka zarez ne samo zarez
//		if (c % 2 == 1) // u if uvijek ide == jer je to operator usporedivanja
//			printf("%d\n", c); //tip podataka za ispis se okruzujue dvostrukim navodnicima nakon kojih ide zarez
//	return 0; // nedostaje razmak izmedu return i 0
//}

//2. zadatak
//int main() {
//
//	int n;
//	do {
//		scanf("%d", &n);
//	} while (n < 5 || n>25);
//
//	for (int i = 1; i <= n; i++) {
//		for (int j = 1; j <= i; j++) {
//			printf("*");
//		}
//		printf("\n");
//	}
//
//
//	return 0;
//}


//3. zadatak

//int main() {
//
//	char slovo;
//	int br = 0;
//	do {
//		scanf("%c", &slovo); getchar();
//		switch (slovo) {
//		case 'a':
//			br++; break;
//		case 'b':
//			br++; break;
//		case 'c':
//			br++; break;
//		case 'd':
//			br++; break;
//		case 'e':
//			br++; break;
//		case 'f':
//			br++; break;
//		case 'A':
//			br++; break;
//		case 'B':
//			br++; break;
//		case 'C':
//			br++; break;
//		case 'D':
//			br++; break;
//		case 'E':
//			br++; break;
//		case 'F':
//			br++; break;
//		case '!':
//			break;
//		default:
//			printf("Netocan unos.\n");
//			break;
//		}
//	} while (slovo != '!');
//
//	printf("Uneseno je %d stvari.", br);
//
//	return 0;
//}


//4. zadatak
//ovo dolje nemoj radit pisi int main(void), ali ja se moram nekako zabavit pa eto
//char main/*der*/() {
//
//	int n;
//	scanf("%d", &n);
//
//	while (n != 0) {
//		printf("%d", n % 10);
//		n /= 10;
//		if (n == 0) break;
//		printf("__");
//	}
//
//	return 'a'; //ne mora uvijek biti return 0 just FYI
//}


//5. zadatak
//int main(void) {
//
//	int polje[10];
//	float br = 0;
//	for (int i = 0; i < 10; i++) {
//		scanf("%d", &polje[i]);
//		if (polje[i] % 2 == 0)
//			br++;
//	}
//	printf("Postotak parnih brojeva u polju je %.2f %%.\n", (br / 10) * 100);
//
//	return 0;
//}


//6. zadatak
int main(void) {

	char a, b, c;
	scanf("%c%c%c", &a, &b, &c);
	if (a < b && a < c) {
		if (b < c) {
			printf("%x", c - b);
		}
		else {
			printf("%x", b - c);
		}
	}
	else if (b < a && b < c) {
		if (a < c) {
			printf("%x", c - a);
		}
		else {
			printf("%x", a - c);
		}
	}
	else if (c < a && c < b) {
		if (a < b) {
			printf("%x", b - a);
		}
		else {
			printf("%x", a - b);
		}
	}

	return 0;
}